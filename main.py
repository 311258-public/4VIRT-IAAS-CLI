#!/usr/bin/env python

"""
Entrypoint; parses arguments and exposes various functions
"""

# Hierarchy: http://vmware.github.io/pyvmomi-community-samples/assets/vchierarchy.pdf
# Samples: https://github.com/vmware/pyvmomi-community-samples

import argparse
import logging
import ssl
from pyVim import connect
from pyVmomi import vim

class customerNotFound(Exception):
    def __init__(self, customer):
        self.customer = customer
        self.message = "Couldn't find customer: {}".format(customer)
        super().__init__(self.message)

class datacenterNotFound(Exception):
    def __init__(self, datacenter):
        self.datacenter = datacenter
        self.message = "Couldn't find datacenter: {}".format(datacenter)
        super().__init__(self.message)

class npiaasvmware():
    def __init__(self, server, username, password, customer, debug=False):
        self.logger = logging.getLogger('NP-IAAS')
        if debug:
            self.logger.setLevel(logging.DEBUG)

        # Ignore self signed certs and such
        s = ssl.SSLContext(ssl.PROTOCOL_TLS)
        s.verify_mode = ssl.CERT_NONE

        self.__si = connect.SmartConnect(host=server, user=username, pwd=password, sslContext=s)
        self.logger.debug("Got SI")
        self.__dcname = "4VIRT" # Hardcoded because why not
        self.__datastore = "SharedDatastore" # :)
        self.__cluster = "Virtual ESXis" # :))
        self.customer = customer

    def findCustomerFolder(self):
        content = self.__si.RetrieveContent()
        datacenters = content.rootFolder

        # Find our datacenter
        datacenter = None
        for dc in datacenters.childEntity:
            if dc.name == self.__dcname:
                datacenter = dc
                self.logger.debug('Found DC: {}'.format(datacenter.name))
                break
        # If it failed
        if datacenter is None:
            raise datacenterNotFound(self.__dcname)

        # Only thing that interests us is the customer's VMFolder, let's find it
        customersFolder = None
        for folder in datacenter.vmFolder.childEntity:
            if folder.name == self.customer:
                customersFolder = folder
                self.logger.debug("Found customer's folder: {}".format(customersFolder.name))
                break
        # If it doesn't exist
        if customersFolder is None:
            raise customerNotFound(self.customer)
        else:
            return customersFolder

    def getLastSnapshot(self, snapshots, snapob):
        snap_obj = []
        for snapshot in snapshots:
            if snapshot.snapshot == snapob:
                snap_obj.append(snapshot)
            snap_obj = snap_obj + self.getLastSnapshot(snapshot.childSnapshotList, snapob)
        return snap_obj

    def listVM(self, display=False):
        folder = self.findCustomerFolder()
        infos = []
        for vm in folder.childEntity:
            vminfo = {}
            vminfo['summary'] = vm.summary
            vminfo['name'] = vm.summary.config.name
            vminfo['template'] = vm.summary.config.template
            vminfo['os'] = vm.summary.config.guestFullName
            vminfo['state'] = vm.summary.runtime.powerState
            if vm.summary.guest is not None:
                vminfo['vmtools'] = vm.summary.guest.toolsStatus
                if vm.summary.guest.ipAddress:
                    vminfo['ip'] = vm.summary.guest.ipAddress
                else:
                    vminfo['ip'] = 'None (VMTools needs to be installed)'
            if vm.snapshot:
                vminfo['lastsnap'] = self.getLastSnapshot(vm.snapshot.rootSnapshotList, vm.snapshot.currentSnapshot)
                if lastsnap:
                    print("Last bkp   :", vminfo['lastsnap'][0].createTime.strftime('%c'))
            else:
                vminfo['lastsnap'] = 'Never'

            infos.append(vminfo)

        if display and infos != []:
            for vm in infos:
                print("------------")
                print("Name       :", vm['name'])
                print("Template   :", vm['template'])
                print("OS         :", vm['os'])
                print("State      :", vm['state'])
                print("VMTools    :", vm['vmtools'])
                print("IP         :", vm['ip'])
                print("Last bkp   :", vm['lastsnap'])

        return infos

    def createVM(self, name, address, templateName):
        try:
            folder = self.findCustomerFolder()
        except customerNotFound as e:
            self.logger.debug("Caught exception: {}. Will attempt creation.".format(e, self.customer))
            # Fast track DC fetching, since we know it already exists
            for dc in self.__si.RetrieveContent().rootFolder.childEntity:
                if dc.name == self.__dcname:
                    datacenter = dc
                    break
            datacenter.vmFolder.CreateFolder(self.customer)
            self.logger.debug("Folder creation successful.".format(e, self.customer))
            # May be able to optimize that, but w/e
            folder = self.findCustomerFolder()

        # Get the target datastore
        datastore = None
        content = self.__si.RetrieveContent()
        container = content.viewManager.CreateContainerView(content.rootFolder, [vim.Datastore], True)
        for managed_object_ref in container.view:
            if managed_object_ref.name == self.__datastore:
                datastore = managed_object_ref
                self.logger.debug("Found target datastore: {}".format(datastore.name))
                break
        if datastore is None:
            raise ValueError("Couldn't find datastore: {}".format(self.__datastore))

        # Get the target ESX cluster
        cluster = None
        container = content.viewManager.CreateContainerView(content.rootFolder, [vim.ClusterComputeResource], True)
        for managed_object_ref in container.view:
            if managed_object_ref.name == self.__cluster:
                cluster = managed_object_ref
                self.logger.debug("Found target cluster: {}".format(cluster.name))
                break
        if cluster is None:
            raise ValueError("Couldn't find cluster: {}".format(self.__cluster))

        # Get the source template from wherever
        template = None
        container = content.viewManager.CreateContainerView(content.rootFolder, [vim.VirtualMachine], True)
        for managed_object_ref in container.view:
            if managed_object_ref.name == templateName:
                template = managed_object_ref
                self.logger.debug("Found source template: {}".format(template.name))
                break
        if template is None:
            raise ValueError("Couldn't find template: {}".format(templateName))

        # Set relospec & clonespec
        relospec = vim.vm.RelocateSpec()
        relospec.datastore = datastore
        relospec.pool = cluster.resourcePool

        clonespec = vim.vm.CloneSpec()
        clonespec.location = relospec
        clonespec.powerOn = True

        self.logger.debug("Cloning VM")
        template.Clone(folder=folder, name=name, spec=clonespec)


    def restartVM(self, name):
        folder = self.findCustomerFolder()
        okay = False
        for vm in folder.childEntity:
            if vm.summary.config.name == name:
                # Attempt a clean reboot only if tools are installed, reset otherwise
                if vm.summary.guest.toolsStatus == 'toolsNotInstalled':
                    vm.Reset()
                    print('Resetting {}'.format(name))
                else:
                    vm.RebootGuest()
                    print('Restarting {}'.format(name))
                okay = True
        if not okay:
            print("Couldn't find VM '{}' for customer '{}'.".format(name, self.customer))

    def deleteVM(self, name):
        folder = self.findCustomerFolder()
        okay = False
        for vm in folder.childEntity:
            if vm.summary.config.name == name:
                # Task will fail if the VM's on
                if vm.summary.runtime.powerState == 'poweredOn':
                    vm.PowerOff()
                    self.logger.debug("VM was shut down before destruction")
                vm.Destroy()
                print('Deleting {}'.format(name))
                okay = True
        if not okay:
            print("Couldn't find VM '{}' for customer '{}'.".format(name, self.customer))

if __name__ == '__main__':
    logging.basicConfig()
    parser = argparse.ArgumentParser(description='VMWare Environment managing toolkit')
    parser.add_argument('-s', '--server', help='vSphere Server address', required=True)
    parser.add_argument('-u', '--username', help='vSphere username', required=True)
    parser.add_argument('-p', '--password', help='vSphere password', required=True)
    parser.add_argument('-c', '--customer', help='Customer name', required=True)
    parser.add_argument('-d', '--debug', action='store_true', help='Debug mode')

    subparsers = parser.add_subparsers(dest='command', required=True)

    listvm = subparsers.add_parser('list', help="List customer's VMs")

    newvm = subparsers.add_parser('create', help='Create a new VM')
    newvm.add_argument('name', help='New VM name')
    newvm.add_argument('address', help='VM address')
    newvm.add_argument('template', help='Base template')

    restartvm = subparsers.add_parser('restart', help='Restart a specific VM')
    restartvm.add_argument('name', help='VM to restart')

    deletevm = subparsers.add_parser('delete', help='Delete a specific VM')
    deletevm.add_argument('name', help='VM to delete')

    args = parser.parse_args()

    handler = npiaasvmware(server=args.server,
                           username=args.username,
                           password=args.password,
                           customer=args.customer,
                           debug=args.debug)

    if args.command == 'list':
        handler.listVM(display=True)
    elif args.command == 'create':
        handler.createVM(args.name, args.address, args.template)
    elif args.command == 'restart':
        handler.restartVM(args.name)
    elif args.command == 'delete':
        handler.deleteVM(args.name)
    else:
        raise('This should be impossible...?')